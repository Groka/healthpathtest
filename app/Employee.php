<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 */
class Employee extends Model
{
    protected $fillable = ['first_name', 'last_name', 'practice_id', 'email', 'phone'];

    protected $table = 'employees';

    public function practice()
    {
        return $this->belongsTo(Practice::class);
    }
}
