<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string logo
 * @property string name
 * @property Collection fieldsOfPractice
 * @property Collection employees
 */
class Practice extends Model
{
    protected $table = 'practices';

    protected $fillable = ['name', 'email', 'logo', 'website'];

    public function fieldsOfPractice()
    {
        return $this->belongsToMany(FieldOfPractice::class, 'practice_field_of_practice');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
