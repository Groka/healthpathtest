<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 */
class FieldOfPractice extends Model
{
    protected $table = 'fields_of_practice';

    protected $fillable = ['tag'];

    function practices()
    {
        return $this->belongsToMany(Practice::class, 'practice_field_of_practice');
    }
}