<?php

namespace App\Http\Controllers;

use App\Employee;
use App\FieldOfPractice;
use App\Http\Requests\PracticeRequest;
use App\Practice;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class PracticeController extends Controller
{
    /**
     * PracticeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): Response
    {
        $practices = Practice::paginate(10);

        return \response()->view('practices.index', compact('practices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(): Response
    {
        $employees = Employee::all();
        $fieldsofpractice = FieldOfPractice::all();
        return \response()->view('practices.create', compact('employees', 'fieldsofpractice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PracticeRequest $request
     * @return RedirectResponse
     */
    public function store(PracticeRequest $request): RedirectResponse
    {
        $data = $request->except('_token');
//        dd($request->all());
        $path = '';
        if ($request->file('logo')->isValid())
        {
            $path = 'storage/' . $request->logo->store('logos', 'public');
        }

        $data['logo'] = $path;

        /** @var Practice $practice */
        $practice = Practice::create($data);

        $practice->fieldsOfPractice()->sync($data['tags']);

        return redirect()->route('practices.index')->with('info_message', "Practice '$practice->name' successfully added.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function show(Practice $practice): Response
    {
        return \response()->view('practices.show', compact('practice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function edit(Practice $practice): Response
    {
        $employees = ['NONE' => null] + Employee::select(['id', \DB::raw('CONCAT(first_name, \' \', last_name) as name')])->get()->pluck('id', 'name')->toArray();
        $fieldsofpractice = ['NONE' => null] + FieldOfPractice::all()->pluck('id', 'tag')->toArray();

//        dd($fieldsofpractice);

        $owningTags = $practice->fieldsOfPractice->pluck('id')->toArray();
        $owningEmployees = $practice->employees->pluck('id')->toArray();

        return \response()->view('practices.edit',
            compact('practice', 'employees', 'fieldsofpractice', 'owningTags', 'owningEmployees')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PracticeRequest $request
     * @param  \App\Practice $practice
     * @return RedirectResponse
     */
    public function update(PracticeRequest $request, Practice $practice): RedirectResponse
    {
        $data = $request->except('_token');
//        dd($request->all());
        if ($request->has('logo') && $request->file('logo')->isValid())
        {
            // TODO: Fix
            $logoPathSplit = explode('/', $practice->logo);
            if (count($logoPathSplit) > 0)
            {
                Storage::disk('public')->delete('/' . $practice->logo /**logoPathSplit[count($logoPathSplit) - 1]*/);
            }

            $data['logo'] = 'storage/' . $request->logo->store('logos', 'public');
        }
        else
        {
            unset($data['logo']);
        }

        $practice->update($data);

        if (isset($data['tags']))
        {
            $practice->fieldsOfPractice()->sync(\in_array(null, $data['tags']) ? [] : $data['tags']);
        }

//        if (isset($data['employees']))
//        {
//            $practice->employees()->update($data['employees']);
////            $practice->employees()->saveMany(\in_array(null, $data['employees']) ? [] : Employee::where('id', $data['employees']));
//        }

        return redirect()->route('practices.index')->with('info_message', "Practice '$practice->name' successfully edited.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Practice $practice
     * @return RedirectResponse
     */
    public function destroy(Practice $practice): RedirectResponse
    {
        // TODO: Fix logo delete
        Storage::disk('public')->delete(explode('/', $practice->logo)[2]);
        Practice::destroy($practice->id);
        return redirect()->route('practices.index')->with('info_message', "Practice #$practice->id deleted successfully");
    }
}
