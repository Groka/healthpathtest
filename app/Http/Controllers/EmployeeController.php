<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\EmployeeRequest;
use App\Practice;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * EmployeeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::paginate(10);

        return response()->view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $practices = ['NONE' => null] + Practice::all()->pluck('id', 'name')->toArray();
        return response()->view('employees.create', compact('practices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmployeeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $data = $request->except('_token');

        Employee::create($data);

        return redirect()->route('employees.index')->with('info_message', 'New employee successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return response()->view('employees.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $practices = ['NONE' => null] + Practice::all()->pluck('id', 'name')->toArray();
        return response()->view('employees.edit', compact('employee', 'practices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmployeeRequest $request
     * @param  \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        $data = $request->except('_token');

        $employee->update($data);

        return redirect()->route('employees.index')->with('info_message', 'Employee successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $message = 'Employee successfully deleted.';
        if (Employee::destroy($employee->id) == 0)
        {
            $message = 'Something went wrong. Employee couldn\'t be deleted.';
        }

        return redirect()->route('employees.index')->with('info_message', $message);
    }
}
