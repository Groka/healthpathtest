<?php

namespace App\Http\Controllers;

use App\FieldOfPractice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FieldOfPracticeController extends Controller
{
    /**
     * FieldOfPracticeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tags = FieldOfPractice::paginate(10);

        return \response()->view('fieldsofpractice.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return \response()->view('fieldsofpractice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $newTag = FieldOfPractice::create($data);

        return redirect()->route('fieldsofpractice.index')->with('info_message', 'Field of practice successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param FieldOfPractice $fieldsofpractice
     * @return Response
     */
    public function show(FieldOfPractice $fieldsofpractice): Response
    {
        return \response()->view('fieldsofpractice.show', ['tag' => $fieldsofpractice]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FieldOfPractice $fieldsofpractice
     * @return Response
     */
    public function edit(FieldOfPractice $fieldsofpractice)
    {
        return response()->view('fieldsofpractice.edit', ['tag' => $fieldsofpractice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param FieldOfPractice $fieldsofpractice
     * @return Response
     */
    public function update(Request $request, FieldOfPractice $fieldsofpractice)
    {
        $data = $request->except('_token');
        $fieldsofpractice->update($data);

        return redirect()->route('fieldsofpractice.index')->with('info_message', 'Field of practice successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param FieldOfPractice $fieldsofpractice
     * @return Response
     */
    public function destroy(FieldOfPractice $fieldsofpractice)
    {
        $result = FieldOfPractice::destroy($fieldsofpractice->id);

        $message = 'Field of practice successfully deleted.';
        if ($result == 0)
        {
            $message = 'Oops. Something went wrong. Record couldn\'t be deleted';
        }

        return redirect()->route('fieldsofpractice.index')->with('info_message', $message);
    }
}
