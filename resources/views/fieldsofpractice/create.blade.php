@extends('layouts.app')
@section('content')
    <div class="px-5">
        <h1 class="text-center mb-4">Create New Field Of Practice</h1>
        <form method="POST" action="{{ route('fieldsofpractice.store') }}" enctype="multipart/form-data">
            @csrf
            @include('fieldsofpractice.form')
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection