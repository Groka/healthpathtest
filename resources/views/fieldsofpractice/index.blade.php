@extends('layouts.app')
@section('content')
    <div class="px-5">
        <div class="d-flex justify-content-between mb-4">
            <a href="{{ route('fieldsofpractice.create') }}" class="btn btn-success btn-lg" title="Add New Field Of Practice">
                Add New Field Of Practice
            </a>
            {{ $tags->links() }}
        </div>
        <div class="table-responsive">
            <table class="table table-stripped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tag</th>
                    <th scope="col">Created at</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tags as $tag)
                    <tr>
                        <th scope="row">{{ $tag->id }}</th>
                        <td>{{ $tag->tag }}</td>
                        <td>{{ $tag->created_at }}</td>
                        <td class="btn-group">
                            <a href="{{ route('fieldsofpractice.show', $tag->id) }}" class="btn btn-success btn-sm">View</a>
                            <a href="{{ route('fieldsofpractice.edit', $tag->id) }}" class="btn btn-primary btn-sm">Edit</a>
                            <form method="post" action="{{ route('fieldsofpractice.destroy', $tag->id) }}" style="display:inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-center">
            {{ $tags->links() }}
        </div>

    </div>
@endsection