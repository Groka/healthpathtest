@extends('layouts.app')
@section('content')

    <div class="px-5">
        <div class="d-flex justify-content-start">
            <a href="{{ route('fieldsofpractice.edit', $tag->id) }}" class="btn btn-primary">Edit</a>
            &nbsp;
            <form method="post" action="{{ route('fieldsofpractice.destroy', $tag->id) }}">
                @csrf
                {{ method_field('DELETE') }}
                <button class="btn btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
            </form>
        </div>
        <br>

        <div class="table-responsive">
            <table class="table table-stripped">
                <tbody>
                    <tr>
                        <th scope="row">ID</th>
                        <th>{{ $tag->id }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Tag</th>
                        <th>{{ $tag->tag }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Practices with this tag</th>
                        <th>
                            <?php
                                echo e(implode(', ', $tag->practices->pluck('name')->toArray()));
                            ?>
                        </th>
                    </tr>
                    <tr>
                        <th scope="row">Created_at</th>
                        <th>{{ $tag->created_at }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Updated at</th>
                        <th>{{ $tag->updated_at }}</th>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
@endsection