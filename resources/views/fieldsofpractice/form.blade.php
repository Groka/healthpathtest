<div class="form-row">
    <div class="form-group col-md-6">
        <label for="tag">Tag</label>
        <input name="tag" type="text" class="form-control @error('tag') is-invalid @enderror" id="tag"
               value="{{ isset($tag) ? $tag->tag : old('tag') }}">
        @if ($errors->has('tag'))
            <div class="invalid-feedback">
                @foreach($errors->get('tag') as $message)
                    {{ $message }} <br>
                @endforeach
            </div>
        @endif
    </div>
</div>

