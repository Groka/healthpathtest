@extends('layouts.app')
@section('content')

    <div class="px-5">
        <div class="d-flex justify-content-start">
            <a href="{{ route('practices.edit', $practice->id) }}" class="btn btn-primary">Edit</a>
            &nbsp;
            <form method="post" action="{{ route('practices.destroy', $practice->id) }}">
                @csrf
                {{ method_field('DELETE') }}
                <button class="btn btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
            </form>
        </div>
        <br>

        <div class="table-responsive">
            <table class="table table-stripped">
                <tbody>
                <tr>
                    <th scope="row">ID</th>
                    <th>{{ $practice->id }}</th>
                </tr>
                <tr>
                    <th scope="row">Name</th>
                    <th>{{ $practice->name }}</th>
                </tr>
                <tr>
                    <th scope="row">Email</th>
                    <th>{{ $practice->email }}</th>
                </tr>
                <tr>
                    <th scope="row">Website</th>
                    <th><a href="{{ $practice->website }}">{{ $practice->website }}</a></th>
                </tr>
                <tr>
                    <th scope="row">Created_at</th>
                    <th>{{ $practice->created_at }}</th>
                </tr>
                <tr>
                    <th scope="row">Updated at</th>
                    <th>{{ $practice->updated_at }}</th>
                </tr>
                <tr>
                    <th scope="row">Logo</th>
                    <th><img src="{{ $practice->logo }}" class="img-fluid" alt="Responsive image"></th>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
@endsection