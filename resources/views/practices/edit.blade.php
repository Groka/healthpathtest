@extends('layouts.app')
@section('content')
    <div class="px-5">
        <h1 class="text-center mb-4">Edit Practice #{{ $practice->id }}</h1>
        <form method="POST" action="{{ route('practices.update', $practice->id) }}" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            @include('practices.form')
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection