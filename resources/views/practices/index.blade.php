@extends('layouts.app')
@section('content')
    <div class="px-5">
        <div class="d-flex justify-content-between mb-4">
            <a href="{{ route('practices.create') }}" class="btn btn-success btn-lg" title="Add New Practice">
                Add New Practice
            </a>
            {{ $practices->links() }}
        </div>
        <div class="table-responsive">
            <table class="table table-stripped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Logo</th>
                    <th scope="col">Website</th>
                    {{--<th scope="col">Created at</th>--}}
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($practices as $practice)
                    <tr>
                        <th scope="row">{{ $practice->id }}</th>
                        <td>{{ $practice->name }}</td>
                        <td>{{ $practice->email }}</td>
                        <td><img src="{{ Str::startsWith($practice->logo, 'http') ? $practice->logo : url('/') . '/' . $practice->logo }}" class="img-thumbnail" width="100" height="100"></td>
                        <td><a href="{{ $practice->website }}">{{ Str::limit($practice->website, 20) }} </a></td>
                        {{--<td>{{ $practice->created_at }}</td>--}}
                        <td class="btn-group-">
                            <a href="{{ route('practices.show', $practice->id) }}" class="btn btn-success btn-sm">View</a>
                            <a href="{{ route('practices.edit', $practice->id) }}" class="btn btn-primary btn-sm">Edit</a>
                            <form method="post" action="{{ route('practices.destroy', $practice->id) }}" style="display:inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-center">
            {{ $practices->links() }}
        </div>

    </div>
@endsection