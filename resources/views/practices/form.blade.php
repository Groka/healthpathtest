<div class="form-row">
    <div class="form-group col-md-6">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="name"
               value="{{ isset($practice) ? $practice->name : old('name') }}">
        @if ($errors->has('name'))
            <div class="invalid-feedback">
                @foreach($errors->get('name') as $message)
                    {{ $message }} <br>
                @endforeach
            </div>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="email">Email</label>
        <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="email"
               value="{{ isset($practice) ? $practice->email : old('email') }}">
        @if ($errors->has('email'))
            <div class="invalid-feedback">
                @foreach($errors->get('email') as $message)
                    {{ $message }} <br>
                @endforeach
            </div>
        @endif
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="website">Website</label>
        <input name="website" type="url" class="form-control" id="website" value="{{ isset($practice) ? $practice->website : old('website') }}">
    </div>
    <div class="form-group {{ isset($practice) ? 'col-md-3' : 'col-md-6' }}">
        <label for="logo">Logo</label>
        <input name="logo" type="file" class="form-control-file @error('logo') is-invalid @enderror" id="logo">
        @if ($errors->has('logo'))
            <div class="invalid-feedback">
                Logo must be at least 100x100
            </div>
        @endif
    </div>
    @if(isset($practice) && $practice->logo !== '')
        <div class="form-group col-md-3">
            <label for="current-logo">Current Logo</label>
            <img src="{{ Str::startsWith($practice->logo, 'http') ? $practice->logo : url('/') . '/' . $practice->logo }}" class="img-thumbnail" height="100" width="100">
        </div>
    @endif
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="tags">Fields of practice</label>
        <select multiple class="form-control" id="tags" name="tags[]" size="10">
            @foreach($fieldsofpractice as $tag => $id)
                <option value="{{ $id }}" @if (isset($practice) && \in_array($id, $owningTags)) selected @endif>{{ $tag }}</option>
            @endforeach
        </select>
    </div>
    @isset ($practice)
        <div class="form-group col-md-6">
            <label for="employees">Employees</label>
            <select multiple class="form-control" id="employees" name="employees[]" size="10" readonly>
                @foreach($employees as $employee => $id)
                    <option value="{{ $id }}" @if(\in_array($id, $owningEmployees)) selected @endif>{{ $employee }}</option>
                @endforeach
            </select>
        </div>
    @endisset
</div>

