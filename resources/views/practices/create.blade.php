@extends('layouts.app')
@section('content')
    <div class="px-5">
        <h1 class="text-center mb-4">Create New Practice</h1>
        <form method="POST" action="{{ route('practices.store') }}" enctype="multipart/form-data">
            @csrf
            @include('practices.form')
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection