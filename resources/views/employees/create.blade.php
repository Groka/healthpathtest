@extends('layouts.app')
@section('content')
    <div class="px-5">
        <h1 class="text-center mb-4">Create New Employee</h1>
        <form method="POST" action="{{ route('employees.store') }}" enctype="multipart/form-data">
            @csrf
            @include('employees.form')
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection