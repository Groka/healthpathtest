@extends('layouts.app')
@section('content')

    <div class="px-5">
        <div class="d-flex justify-content-start">
            <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-primary">Edit</a>
            &nbsp;
            <form method="post" action="{{ route('employees.destroy', $employee->id) }}">
                @csrf
                {{ method_field('DELETE') }}
                <button class="btn btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
            </form>
        </div>
        <br>

        <div class="table-responsive">
            <table class="table table-stripped">
                <tbody>
                    <tr>
                        <th scope="row">ID</th>
                        <th>{{ $employee->id }}</th>
                    </tr>
                    <tr>
                        <th scope="row">First Name</th>
                        <th>{{ $employee->first_name }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Last Name</th>
                        <th>{{ $employee->last_name }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Practice</th>
                        <th>
                            @if ($employee->practice)
                                <a href="{{ route('practices.show', $employee->practice->id) }}">{{ $employee->practice->name }} </a>
                            @else
                                /
                            @endif
                        </th>
                    </tr>
                    <tr>
                        <th scope="row">Email</th>
                        <th>{{ $employee->email }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Created_at</th>
                        <th>{{ $employee->created_at }}</th>
                    </tr>
                    <tr>
                        <th scope="row">Updated at</th>
                        <th>{{ $employee->updated_at }}</th>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
@endsection