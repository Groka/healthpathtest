<div class="form-row">
    <div class="form-group col-md-6">
        <label for="first_name">First Name</label>
        <input name="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name"
               value="{{ isset($employee) ? $employee->first_name : old('first_name') }}">
        @if ($errors->has('first_name'))
            <div class="invalid-feedback">
                @foreach($errors->get('first_name') as $message)
                    {{ $message }} <br>
                @endforeach
            </div>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="last_name">Last Name</label>
        <input name="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name"
               value="{{ isset($employee) ? $employee->last_name : old('last_name') }}">
        @if ($errors->has('last_name'))
            <div class="invalid-feedback">
                @foreach($errors->get('last_name') as $message)
                    {{ $message }} <br>
                @endforeach
            </div>
        @endif
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="email">Email</label>
        <input name="email" type="email" class="form-control" id="email"
               value="{{ isset($employee) ? $employee->email : old('email') }}">
    </div>
    <div class="form-group col-md-6">
        <label for="phone">Phone</label>
        <input name="phone" type="tel" class="form-control" id="phone"
               value="{{ isset($employee) ? $employee->phone : old('phone') }}">
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="practice_id">Practice</label>
        <select class="form-control @error('practice_id') is-invalid @enderror" id="practice_id" name="practice_id">
            @foreach($practices as $name => $id)
                <option value="{{ $id }}" @if (isset($employee) && $employee->practice && $id === $employee->practice->id) selected @endif>{{ $name }}</option>
            @endforeach
        </select>
        @if ($errors->has('practice_id'))
            <div class="invalid-feedback">
                @foreach($errors->get('practice_id') as $message)
                    {{ $message }} <br>
                @endforeach
            </div>
        @endif
    </div>
</div>

