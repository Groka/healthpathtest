@extends('layouts.app')
@section('content')
    <div class="px-5">
        <div class="d-flex justify-content-between mb-4">
            <a href="{{ route('employees.create') }}" class="btn btn-success btn-lg" title="Add New Employee">
                Add New Employee
            </a>
            {{ $employees->links() }}
        </div>
        <div class="table-responsive">
            <table class="table table-stripped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Practice</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <th scope="row">{{ $employee->id }}</th>
                        <td>{{ $employee->first_name }}</td>
                        <td>{{ $employee->last_name }}</td>
                        <td>
                            @if ($employee->practice)
                                <a href="{{ route('practices.show', $employee->practice->id) }}">{{ $employee->practice->name }} </a>
                            @else
                            /
                            @endif
                        </td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td class="btn-group-">
                            <a href="{{ route('employees.show', $employee->id) }}" class="btn btn-success btn-sm">View</a>
                            <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-primary btn-sm">Edit</a>
                            <form method="post" action="{{ route('employees.destroy', $employee->id) }}" style="display:inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Confirm delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex justify-content-center">
            {{ $employees->links() }}
        </div>

    </div>
@endsection