Test assignement for Healtpath.

Laravel 6, CRUD, factories, migrations, seeds.

### NOTE

By default 'FieldsOfPracticeSeeder' and 'PracticesSeeder' are commented out in DatabaseSeeder::run method. If you want to have some dummy data just uncomment.