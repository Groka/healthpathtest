<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePracticefieldofpracticeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_field_of_practice', function (Blueprint $table) {
//            $table->bigIncrements('id');
            $table->bigInteger('practice_id')->unsigned();
            $table->foreign('practice_id')->references('id')->on('practices')->onDelete('CASCADE');
            $table->bigInteger('field_of_practice_id')->unsigned();
            $table->foreign('field_of_practice_id')->references('id')->on('fields_of_practice')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practice_field_of_practice');
    }
}
