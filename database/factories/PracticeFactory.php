<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Practice;
use Faker\Generator as Faker;

$factory->define(Practice::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->unique()->companyEmail,
        'logo' => $faker->imageUrl(100, 100),
        'website' => $faker->url
    ];
});
