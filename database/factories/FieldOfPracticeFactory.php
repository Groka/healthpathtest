<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\FieldOfPractice;
use Faker\Generator as Faker;

$factory->define(FieldOfPractice::class, function (Faker $faker) {
    return [
        'tag' => $faker->word
    ];
});
