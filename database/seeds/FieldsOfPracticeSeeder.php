<?php

use App\FieldOfPractice;
use Illuminate\Database\Seeder;

class FieldsOfPracticeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FieldOfPractice::class, 50)->create();
    }
}
