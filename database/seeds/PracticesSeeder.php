<?php

use App\Employee;
use App\FieldOfPractice;
use App\Practice;
use Illuminate\Database\Seeder;

class PracticesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Practice::class, 50)->create()->each(function(Practice $practice) {
            $practice->employees()->saveMany(factory(Employee::class, 5)->make());
            $practice->fieldsOfPractice()->saveMany(FieldOfPractice::all()->random(3));
        });
    }
}
